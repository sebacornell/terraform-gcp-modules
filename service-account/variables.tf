variable "account_id" {
  type = string
}

variable "display_name" {
  type = string
}

variable "description" {
  type = string
}

variable "bindings" {
  type = map(object({
    project  = string
    role = string
  }))
  default = {}
}
