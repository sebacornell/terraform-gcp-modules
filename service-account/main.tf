#######################################################
# SERVICE ACCOUNTS
#######################################################

resource "google_service_account" "account" {
  account_id   = var.account_id
  display_name = var.display_name
  description  = var.description 
}

resource "google_project_iam_member" "binding" {
  for_each = var.bindings  
  project  = each.value.project
  role     = each.value.role
  member   = "serviceAccount:${google_service_account.account.email}"
}

output "name" {
   value = google_service_account.account.name
}

output "mail" {
   value = google_service_account.account.email
}
