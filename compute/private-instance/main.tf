resource "google_compute_instance" "vm" {
  name         = var.compute_name
  machine_type = var.compute_machine_type
  zone         = var.compute_region_zone

  tags = var.compute_tags

  boot_disk {
    initialize_params {
      image = var.compute_boot_disk_image
      size  = var.compute_boot_disk_size
    }
  }
  network_interface {
    network    = var.compute_network
    subnetwork = var.compute_subnetwork
  }
  shielded_instance_config {
    enable_secure_boot          = var.compute_enable_secure_boot
    enable_vtpm                 = var.compute_enable_vtpm
    enable_integrity_monitoring = var.compute_enable_integrity_monitoring
  }

  metadata = var.compute_metadata

  metadata_startup_script = var.compute_startup_script

  service_account {
    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    email  = var.compute_service_account
    scopes = var.compute_service_account_scopes
  }

  depends_on = [
    var.compute_depends_on
  ]
}
