variable "compute_name" {
  type = string
}
variable "compute_machine_type" {
  type = string
}
variable "compute_region_zone" {
  type = string
}
variable "compute_tags" {
  type    = list(string)
  default = []
}
variable "compute_boot_disk_image" {
  type = string
}
variable "compute_boot_disk_size" {
  type = number
}
variable "compute_network" {
  type    = string
  default = "default"
}
variable "compute_subnetwork" {
  type    = string
  default = "default"
}
variable "compute_enable_secure_boot" {
  type    = bool
  default = false
}
variable "compute_enable_vtpm" {
  type    = bool
  default = false
}
variable "compute_enable_integrity_monitoring" {
  type    = bool
  default = false
}
variable "compute_metadata" {
  type    = map(string)
  default = {}
}
variable "compute_startup_script" {
  type    = string
  default = null
}
variable "compute_service_account" {
  type = string
}
variable "compute_service_account_scopes" {
  type    = list(string)
  default = []
}
variable "compute_depends_on" {
  description = "Dependencias de Compute"
  default     = []
}